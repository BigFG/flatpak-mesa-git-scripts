# flatpak-mesa-git-scripts

Scripts for building flatpak mesa-git easily for x86

## Getting started

This Repo was tested on Fedore 36 and assumes you have the needed dependencies for building
https://gitlab.com/freedesktop-sdk/mesa-git-extension

You would be wise to build this on Fedora or in a Fedora VM, my installed packages are 

sudo dnf install buildstream

sudo dnf install make cmake meson git

sudo dnf install flatpak-builder

sudo dnf install bst-external

I cannot tell you what your distro package names may be or what you may need, on other distros youre on your own for dependencies 

if you wish to add codecs to the build youll need to add the line (with the desired codecs if not included here)

`-Dvideo-codecs=h264dec,h264enc,h265dec,h265enc,vc1dec`

to the `meson-local: >-` section of the file `mesa.bst` in the `elements` folder. You can place it right under

`-Dzstd=enabled`

## How to use

you will need to put these scripts in the root of the repo folder and make sure theyre executable

Once you have the needed dependencies execute the scripts from terminal in numbered order in the folder you downloaded/extracted mesa-git-extension repo above to

i.e on how to execute the script

`./1_prepare.sh`

1_prepare.sh will take a while, wait till its done to execute the next script


once you have executed 3_export.sh you will have 2 folders

**mesa-gitGL32**

**mesa-gitGL64**


if youre on the target machine execute add-repo.sh, if not move those folders to the target machine. In the directory that contains both folders then execute add-repo.sh


if you need to remove the 2 repos execute remove-repo.sh


you can now install mesa-git with

`flatpak install mesa-git `

and select the GL64 or GL32 repo to install the 32 and 64bit versions.
